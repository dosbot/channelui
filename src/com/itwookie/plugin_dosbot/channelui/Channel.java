package com.itwookie.plugin_dosbot.channelui;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.events.chat.RoomChangeEvent;
import com.itwookie.dosbot.exceptions.InsufficientPermissionException;
import com.itwookie.dosbot.twitch.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class Channel {

    private JFrame frame;
    private JList list1;
    private JPanel panel1;
    private JLabel lblChatter;
    private JCheckBox chkFollowermode;
    private JSpinner spnSlowSeconds;
    private JLabel lblRoomID;
    private JCheckBox chkSubmode;
    private JCheckBox chkR9Kmode;
    private JCheckBox chkEmotemode;
    private JSpinner spnFollowMonths;
    private JCheckBox chkSlowmode;
    private JProgressBar progressBar1;
    private UserListModel userList;

    private int chatter = 0, maxChatter = 0;

    private boolean mode_emote = false;
    private boolean mode_follower = false;
    private boolean mode_slow = false;
    private boolean mode_sub = false;
    private boolean mode_r9k = false;

    public Channel() {

        frame = new JFrame("Twitch Channel: " + TwitchClient.getChannel().getName());
//        frame = new JFrame("Twitch Channel: "+"DEMO");
        frame.setSize(new Dimension(300, 800));
        frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        frame.setMinimumSize(new Dimension(300, 400));

        userList = new UserListModel();
        list1.setModel(userList);
        list1.setCellRenderer(new TwitchUserRenderer());
        frame.add(panel1);

        spnFollowMonths.setModel(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
        spnSlowSeconds.setModel(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));

        update(TwitchClient.getChannel());
        chkFollowermode.addChangeListener(e -> {
            if (chkFollowermode.isSelected() == mode_follower) return;
            try {
                if (mode_follower = chkFollowermode.isSelected())
                    TwitchClient.getChannel().setFollowerOnly((Integer) spnFollowMonths.getModel().getValue());
                else
                    TwitchClient.getChannel().setFollowerOnly(-1);
                spnFollowMonths.setEnabled(mode_follower);
            } catch (InsufficientPermissionException exception) {
                Plugin.logger.warn(exception.getMessage());
            }
        });
        spnFollowMonths.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        TwitchClient.getChannel().setFollowerOnly((Integer) spnFollowMonths.getModel().getValue());
                    } catch (InsufficientPermissionException exception) {
                        Plugin.logger.warn(exception.getMessage());
                    }
                }
            }
        });
        chkSlowmode.addChangeListener(e -> {
            if (chkSlowmode.isSelected() == mode_slow) return;
            try {
                if (mode_slow = chkSlowmode.isSelected())
                    TwitchClient.getChannel().setSlowMode((Integer) spnSlowSeconds.getModel().getValue());
                else
                    TwitchClient.getChannel().setSlowMode(0);
                spnSlowSeconds.setEnabled(mode_slow);
            } catch (InsufficientPermissionException exception) {
                Plugin.logger.warn(exception.getMessage());
            }
        });
        spnSlowSeconds.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        TwitchClient.getChannel().setSlowMode((Integer) spnSlowSeconds.getModel().getValue());
                    } catch (InsufficientPermissionException exception) {
                        Plugin.logger.warn(exception.getMessage());
                    }
                }
            }
        });
        chkSubmode.addChangeListener(e -> {
            if (chkSubmode.isSelected() == mode_sub) return;
            try {
                TwitchClient.getChannel().setSubOnly(mode_sub = chkSubmode.isSelected());
            } catch (InsufficientPermissionException exception) {
                Plugin.logger.warn(exception.getMessage());
            }
        });
        chkR9Kmode.addChangeListener(e -> {
            if (chkR9Kmode.isSelected() == mode_r9k) return;
            try {
                TwitchClient.getChannel().setR9Kmode(mode_r9k = chkR9Kmode.isSelected());
            } catch (InsufficientPermissionException exception) {
                Plugin.logger.warn(exception.getMessage());
            }
        });
        chkEmotemode.addChangeListener(e -> {
            if (chkEmotemode.isSelected() == mode_emote) return;
            try {
                TwitchClient.getChannel().setEmoteOnly(mode_emote = chkEmotemode.isSelected());
            } catch (InsufficientPermissionException exception) {
                Plugin.logger.warn(exception.getMessage());
            }
        });

        frame.pack();
    }

    public void show() {
        if (!frame.isVisible())
            frame.setVisible(true);
        focus();
    }

    public void update(com.itwookie.dosbot.twitch.Channel channel) {
        boolean isMod = TwitchClient.getSelf().isMod();

        lblRoomID.setText(String.valueOf(channel.getRoomID()));

        Optional<Integer> followMonths = channel.getFollowerOnly();
        if (mode_follower = followMonths.isPresent()) {
            chkFollowermode.setSelected(true);
            spnFollowMonths.getModel().setValue(followMonths.get());
            spnFollowMonths.setEnabled(isMod);
        } else {
            chkFollowermode.setEnabled(false);
            spnFollowMonths.setEnabled(false);
        }

        int slowMode = channel.getSlow();
        if (mode_slow = (slowMode > 0)) {
            chkSlowmode.setSelected(true);
            spnSlowSeconds.getModel().setValue(slowMode);
            spnSlowSeconds.setEnabled(isMod);
        } else {
            chkSlowmode.setSelected(false);
            spnSlowSeconds.setEnabled(false);
        }

        chkFollowermode.setEnabled(isMod);
        chkSlowmode.setEnabled(isMod);

        chkSubmode.setSelected(mode_sub = channel.isSubsOnly());
        chkSubmode.setEnabled(isMod);

        chkR9Kmode.setSelected(mode_r9k = channel.isR9k());
        chkR9Kmode.setEnabled(isMod);

        chkEmotemode.setSelected(mode_emote = channel.isEmoteOnly());
        chkEmotemode.setEnabled(isMod);
    }

    public void update(RoomChangeEvent event) {
        boolean isMod = TwitchClient.getSelf().isMod();

        if (event.getFollowerOnly().changed()) {
            int val = event.getFollowerOnly().getCurrent();
            if (mode_follower = val >= 0) {
                chkFollowermode.setSelected(true);
                spnFollowMonths.getModel().setValue(val);
                spnFollowMonths.setEnabled(isMod);
            } else {
                chkFollowermode.setSelected(false);
                spnFollowMonths.setEnabled(false);
            }
        }

        if (event.getSlow().changed()) {
            int val = event.getSlow().getCurrent();
            if (mode_slow = val > 0) {
                chkSlowmode.setSelected(true);
                spnSlowSeconds.getModel().setValue(val);
                spnSlowSeconds.setEnabled(isMod);
            } else {
                chkSlowmode.setSelected(false);
                spnSlowSeconds.setEnabled(false);
            }
        }

        chkFollowermode.setEnabled(isMod);
        chkSlowmode.setEnabled(isMod);

        if (event.getSubOnly().changed()) {
            chkSubmode.setSelected(mode_sub = event.getSubOnly().getCurrent());
            chkSubmode.setEnabled(isMod);
        }

        if (event.getR9k().changed()) {
            chkR9Kmode.setSelected(mode_r9k = event.getR9k().getCurrent());
            chkR9Kmode.setEnabled(isMod);
        }

        if (event.getEmoteOnly().changed()) {
            chkEmotemode.setSelected(mode_emote = event.getEmoteOnly().getCurrent());
            chkEmotemode.setEnabled(isMod);
        }
    }

    public void addUser(User user) {
        if (!userList.add(user)) return;

        chatter++;
        if (chatter > maxChatter) {
            maxChatter = chatter;
            progressBar1.setMaximum(maxChatter);
            progressBar1.setToolTipText("Maximum: " + maxChatter);
        }
        progressBar1.setValue(chatter);
        lblChatter.setText(String.valueOf(chatter));
    }

    public void removeUser(User user) {
        userList.remove(user);

        chatter = Math.max(0, chatter - 1);
        progressBar1.setValue(chatter);
        lblChatter.setText(String.valueOf(chatter));
    }

    public void update(Collection<User> online) {
        if (userList.updateBlock.get() ||
            userList.listBlocked.getAndSet(true))
            return;
        Set<ReducedUser> newUsers;
        Set<ReducedUser> droppedUsers;

        Set<ReducedUser> reduced = online.stream()
                .map(ReducedUser::new)
                .collect(Collectors.toSet());

        //everyone online, but not listed
        newUsers = new HashSet<>(reduced);
        newUsers.removeAll(userList.getCurrent());

        //everyone listed, but no longer online
        droppedUsers = new HashSet<>(userList.getCurrent());
        droppedUsers.removeAll(reduced);

        Plugin.logger.yell("New Channel Update");
        if (newUsers.size()<100)
            newUsers.forEach(u->{
                Plugin.logger.log("JOIN ", u.userName);
            });
        else Plugin.logger.log("JOIN {", newUsers.size(), "...}");
        if (droppedUsers.size()<100)
        droppedUsers.forEach(u->{
            Plugin.logger.log("PART ", u.userName);
        });
        else Plugin.logger.log("PART {", droppedUsers.size(), "...}");

        new DiffBuffer(userList, newUsers, droppedUsers).execute();

        chatter += newUsers.size() - droppedUsers.size();
        progressBar1.setValue(chatter);
        lblChatter.setText(String.valueOf(chatter));
    }

    public void focus() {
        frame.setAlwaysOnTop(true);
        frame.setAlwaysOnTop(false);
    }

    public void close() {
        userList.sortGuard.halt();
        frame.dispose();
    }

}
