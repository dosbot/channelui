package com.itwookie.plugin_dosbot.channelui;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.commands.Command;
import com.itwookie.dosbot.commands.CommandExecutor;
import com.itwookie.dosbot.eventBus.Subscribe;
import com.itwookie.dosbot.events.chat.ChattersImportedEvent;
import com.itwookie.dosbot.events.chat.RoomChangeEvent;
import com.itwookie.dosbot.events.chat.UserConnectionEvent;
import com.itwookie.dosbot.events.client.DisconnectEvent;
import com.itwookie.dosbot.events.client.PostConnectEvent;
import com.itwookie.dosbot.twitch.Terminal;
import com.itwookie.dosbot.twitch.User;
import com.itwookie.logger.Logger;
import com.itwookie.pluginloader.PluginContainer;
import com.itwookie.pluginloader.WookiePluginAnnotation;
import com.itwookie.pluginloader.WookiePluginFunction;
import com.itwookie.utils.Stoppable;

import java.util.Collection;

import static com.itwookie.pluginloader.WookiePluginFunction.Type.onLoaded;
import static com.itwookie.pluginloader.WookiePluginFunction.Type.onUnload;

@WookiePluginAnnotation(
        Name = "ChannelUI",
        Description = "Show a UI for all users currently in the channel",
        Author = "DosMike",
        Version = "1.0"
)
public class Plugin {

    static Logger logger = Logger.getLogger("ChannelUI");

    Channel channel;

    @WookiePluginFunction(onLoaded)
    public void onLoaded(PluginContainer me) {
        //could retrieve plugin container here
    }

    @WookiePluginFunction(onUnload)
    public void onUnload() {
    }

    private CommandExecutor uicmd = (caller, args) -> {
        if (caller instanceof Terminal && channel != null) {
            channel.show();
        }
    };

    @Subscribe
    public void onPostConnect(PostConnectEvent event) {
        channel = new Channel();
        TwitchClient.getChannel().getOnline()
                .forEach(u -> channel.addUser(u));
        logger.yell("Use the command channelui to bring up the ui (Terminal only)");
        TwitchClient.getCommands().register(Command.builder("channelui")
                .permission(u->false)
                .execute(uicmd)
                .build());
    }

    @Subscribe
    public void onDisconnect(DisconnectEvent event) {
        channel.close();
    }

    @Subscribe
    public void onUserJoin(UserConnectionEvent event) {
        if (channel == null) {
            logger.err(event.getTarget().getUserName() + " joined before connecting!");
            return;
        }
        if (event instanceof UserConnectionEvent.Join) {
            channel.addUser(event.getTarget());
        } else if (event instanceof UserConnectionEvent.Part) {
            channel.removeUser(event.getTarget());
        }
    }

    @Subscribe
    public void onChattersImported(ChattersImportedEvent event) {
        Collection<User> online = TwitchClient.getChannel().getOnline();
        channel.update(online);
        logger.log("Import completed.");
    }

    @Subscribe
    public void onRoomState(RoomChangeEvent event) {
        if (channel != null)
            channel.update(event);
    }

}
