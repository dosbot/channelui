package com.itwookie.plugin_dosbot.channelui;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import java.util.List;
import java.util.Set;

public class DiffBuffer extends SwingWorker<Void, Integer> {

    private Set<ReducedUser> added, removed;

    private UserListModel list;
    public DiffBuffer(UserListModel list, Set<ReducedUser> added, Set<ReducedUser> removed) {
        this.list = list;
        this.added = added;
        this.removed = removed;
        this.amount = added.size() + removed.size();
//        list.listBlocked.set(true);
    }

    long lastMessage = System.currentTimeMillis();
    int amount;
    @Override
    protected Void doInBackground() throws Exception {
        while (list.updateBlock.get()) Thread.yield();
        added.stream().filter(e->!list.wrapped.contains(e)).forEach(e->{
            if (System.currentTimeMillis()-lastMessage>5000) {
                lastMessage = System.currentTimeMillis();
                publish(amount);
            }
            list.wrapped.add(e);
            if (list.getSize() < 10000)
                list.sort(); //immediate sort
            amount--;
        });
        publish(0, removed.size());
        removed.forEach(e->{
            if (System.currentTimeMillis()-lastMessage>5000) {
                lastMessage = System.currentTimeMillis();
                publish(amount);
            }
            list.wrapped.remove(e);
            amount--;
        });
        return null;
    }

    @Override
    protected void process(List<Integer> chunks) {
        Plugin.logger.log(chunks.get(0)+" changes pending");
    }

    @Override
    protected void done() {
        Plugin.logger.log("Updating list (creating Labels)");
        list.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, list.wrapped.size()-1));
        Plugin.logger.log("All pending changes applied");
        list.changed.set(added.size()>0 || removed.size()>0);
        list.listBlocked.set(false);
    }

}
