package com.itwookie.plugin_dosbot.channelui;

import com.itwookie.dosbot.TwitchClient;
import com.itwookie.dosbot.twitch.User;

import java.awt.*;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;

public class ReducedUser {
    String displayName;
    String userName;
    Color color;
    String tostring;

    public ReducedUser(User channelUser) {
        userName = channelUser.getUserName();
        displayName = channelUser.getDisplayName();
        Optional<User> user = TwitchClient.getChannel().findUser(userName);
        color = user.isPresent() ? user.get().getColor().orElse(Color.lightGray) : Color.lightGray;
        tostring = displayName.equalsIgnoreCase(userName) ? displayName :
                displayName + " (" + userName + ")";
    }

    @Override
    public String toString() {
        return tostring;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReducedUser that = (ReducedUser) o;
        return Objects.equals(userName, that.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(displayName, userName);
    }

    public Color getColor() {
         return color;
    }

    public static class UserComparator implements Comparator<ReducedUser> {
        @Override
        public int compare(ReducedUser o1, ReducedUser o2) {
            return o1.userName.compareTo(o2.userName);
        }
    }
}
