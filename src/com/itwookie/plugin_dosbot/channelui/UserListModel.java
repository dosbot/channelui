package com.itwookie.plugin_dosbot.channelui;

import com.itwookie.dosbot.twitch.User;
import com.itwookie.utils.Stoppable;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class UserListModel implements ListModel<ReducedUser>, ListDataListener {

    List<ReducedUser> wrapped = new LinkedList<>();
//    Set<ReducedUser> wrapped = new TreeSet<>(new ReducedUser.UserComparator());
    private Set<ListDataListener> listener = new HashSet<>();
    AtomicBoolean changed = new AtomicBoolean(false);

    AtomicBoolean listBlocked = new AtomicBoolean(false);
    AtomicBoolean updateBlock = new AtomicBoolean(false);

    private final Comparator c = new ReducedUser.UserComparator();
    void sort() {
        if (wrapped.size()<2) return;
        //unboxed sort algorithm for logging (and speed (but can't actually log))
        Object[] a = wrapped.toArray();
        TimSort.sort(a,0,a.length,c, null, 0, 0);
        ListIterator<ReducedUser> i = wrapped.listIterator();
        for (Object e : a) {
            i.next();
            i.set((ReducedUser) e);
        }
    }

    private static class SortWorker extends SwingWorker<Void, Void> {
        UserListModel model;
        public SortWorker(UserListModel model) {
            this.model = model;
        }

        @Override
        protected Void doInBackground() throws Exception {
            Plugin.logger.log("Sorting...");
//            model.wrapped.sort(new ReducedUser.UserComparator());
            try {
                model.sort();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void done() {
            Plugin.logger.log("Updating list (creating labels)");
            try{
                model.contentsChanged(new ListDataEvent(model, ListDataEvent.CONTENTS_CHANGED, 0, model.wrapped.size()-1));
            } catch (Exception e) {
                e.printStackTrace();
            }
            model.changed.set(false);
            model.updateBlock.set(false);
            Plugin.logger.log("Sort complete");
        }
    }

    Stoppable sortGuard = new Stoppable() {
        private SortWorker worker = null;
        @Override
        public void onLoop() {
            if (getSize() < 10000 && changed.get() && !listBlocked.get() && !updateBlock.getAndSet(true)) {
                worker = new SortWorker(UserListModel.this);
                worker.execute();
            }
            try {
                Thread.sleep(Math.max(2500, getSize()));
            } catch (Exception ignore) {
            }
        }
    };

    public UserListModel() {
        sortGuard.start();
    }

    @Override
    public int getSize() {
        return wrapped.size();
    }

    @Override
    public ReducedUser getElementAt(int index) {
        return wrapped.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        listener.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        listener.remove(l);
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
        listener.forEach(l -> l.intervalAdded(e));
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
        listener.forEach(l -> l.intervalRemoved(e));
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
        listener.forEach(l -> l.contentsChanged(e));
    }

    /** @return true if user was added, false if user was already in set */
    public boolean add(User user) {
        return add(new ReducedUser(user));
    }
    public boolean add(ReducedUser user) {
        boolean added=false;
        if (listBlocked.get() || updateBlock.get()) return false;
//        synchronized (changeMutex) {
        int i = wrapped.indexOf(user);
        if (i<0) {
            int at = wrapped.size();
            if (wrapped.add(user)) {
                added = true;
                changed.set(true);
                intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, at, at));
            }
        } else { //dont tell anyone
            ReducedUser pre = wrapped.get(i);
            if (pre.color != user.color) {
                wrapped.set(i, user);
                changed.set(true);
                intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, i, i));
            }
        }
//            }
        return added;
    }

    public void remove(User user) {
        if (listBlocked.get() || updateBlock.get()) return;
        ReducedUser u = new ReducedUser(user);
//        synchronized (changeMutex) {
        int at = wrapped.indexOf(u);
        if (at >= 0) {
            if (wrapped.remove(u))
                intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, at, at));
        }
        changed.set(true);
//        }
    }
//    public void remove(ReducedUser user) {
////        synchronized (changeMutex) {
//        int at = wrapped.indexOf(user);
//        if (at >= 0) {
//            if (wrapped.remove(user))
//                intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, at, at));
//        }
//        changed.set(true);
////        }
//    }

//    public void diff(Collection<ReducedUser> added, Collection<ReducedUser> removed) {
////        synchronized (changeMutex) {
//        added.forEach(u->{
//            if (!wrapped.contains(u))
//                wrapped.add(u);
//        });
//        removed.forEach(u-> wrapped.remove(u));
//        contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, wrapped.size()-1));
//        changed.set(added.size()>0 || removed.size()>0);
////        }
//    }

    public Collection<ReducedUser> getCurrent() {
//        synchronized (changeMutex) {
        return new HashSet<>(wrapped);
//        }
    }

    public boolean contains(ReducedUser user) {
//        synchronized (changeMutex) {
        return wrapped.contains(user);
//        }
    }

//    public void update(ReducedUser user) {
////        synchronized (changeMutex) {
//        int at = wrapped.indexOf(user);
//        if (at >= 0) {
//            contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, at, at));
//        }
//        changed.set(true);
////        }
//    }
}
