package com.itwookie.plugin_dosbot.channelui;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class TwitchUserRenderer extends JLabel implements ListCellRenderer<ReducedUser> {

    private static final Border focusBorder =
            BorderFactory.createLineBorder(new Color(50, 100, 255), 1);

    public void style(JList list, boolean isSelected, boolean hasFocus) {
//        setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
        setBackground(list.getBackground());
        setBorder(hasFocus ? focusBorder : null);
    }

    public TwitchUserRenderer() {
        setOpaque(true);

    }

    @Override
    public Component getListCellRendererComponent(
            JList<? extends ReducedUser> list,           // the list
            ReducedUser user,            // value to display
            int index,               // cell index
            boolean isSelected,      // is the cell selected
            boolean cellHasFocus)    // does the cell have focus
    {
//        if (index%1000==0)
//            Plugin.logger.log("Building labels ", index, "/", list.getModel().getSize());
        setForeground(user.getColor());
        setText(user.toString());
        style(list, isSelected, cellHasFocus);
        setEnabled(list.isEnabled());
        return this;
    }

}
